#include "../rgit.hpp"

using namespace RGit;

void RGit::assets_handler(Webcore::Call &call)
{
	if(call.get_path().find("/..") != std::string::npos)
	{
		call.response_status = Webcore::NOT_FOUND;
		return;
	}

	try
	{
		std::string filepath(call.get_path());
		if(filepath.front() == '/') filepath.erase(0, 1);

		call.response_body = Webcore::read_file(filepath);

		// TODO File type, etc...
		call.respond();
	}
	catch(const std::exception &)
	{
		call.response_status = Webcore::NOT_FOUND;
	}
}

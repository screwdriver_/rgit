#include "../rgit.hpp"

using namespace RGit;

void RGit::not_found_handler(Webcore::Call &call)
{
	call.response_status = Webcore::NOT_FOUND;
	call.response_body = "TODO: 404";

	call.respond();
}

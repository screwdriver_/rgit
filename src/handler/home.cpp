#include "../rgit.hpp"

using namespace RGit;

static bool is_logged()
{
	// TODO
	return false;
}

void RGit::home_handler(Webcore::Call &call)
{
	call.response_body = Webcore::read_file("templates/home.html");

	const auto &sections = get_instance()->get_sections();
	std::string sections_html;

	for(const auto &s : sections) {
		if(!is_logged() && s.is_private) continue;
		sections_html += s.to_html();
	}

	Webcore::set_tags(call.response_body, "sections", sections_html);
	call.respond();
}

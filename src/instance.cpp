#include "rgit.hpp"

using namespace RGit;

void Instance::register_handlers()
{
	webcore.register_handler("/", home_handler);
	webcore.register_handler("/total_loc", total_loc_handler);

	webcore.register_handler("/repo/*/*", repo_handler);

	webcore.register_handler("/assets/*", assets_handler);

	webcore.register_error_handler(Webcore::NOT_FOUND, not_found_handler);
}

static Section load_section(const std::string &name)
{
	try {
		std::ifstream stream(std::string(DATA_DIR) + name + "/section.json");

		stream.seekg(0, std::ios::end);
		const auto length = stream.tellg();
		stream.seekg(0);

		std::string buffer;
		buffer.resize(length);
		stream.read(&buffer[0], length);

		const auto json = JSON::parse(buffer);
		const auto obj = *((JSON::Object *) json.get());

		Section section;
		section.name = (const char*) *((JSON::Value *) obj["name"].get());
		section.is_private = (bool) *((JSON::Value *) obj["private"].get());
		section.place = (int) *((JSON::Value *) obj["place"].get());

		iterate_dir(std::string(DATA_DIR) + name,
			[&section](const std::string &name)
			{
				section.repos.emplace_back(new Repository(section.name, name));
			});

		return section;
	} catch(const std::exception &) {
		throw std::runtime_error("Failed to read section!");
	}
}

Section *Instance::get_section(const std::string &name)
{
	for(auto &s : sections) {
		if(s.name == name) return &s;
	}

	return nullptr;
}

void Instance::update_sections()
{
	sections.clear();
	iterate_dir(DATA_DIR, [this](const std::string &name)
		{
			sections.push_back(load_section(name));
		});

	sort(sections.begin(), sections.end(),
		[](const Section &s1, const Section &s2)
		{
			return (s1.place < s2.place);
		});
}

var black_background = document.getElementById("black_background");

var counter = document.getElementById("counter");

update_counter();
setInterval(update_counter, 5000);

function open_login()
{
	black_background.style.display = "flex";
}

function intlen(n)
{
	if(n < 10) return 1;
	return 1 + intlen(n / 10);
}

function get_count_str(n)
{
	var len = intlen(n);
	if(len >= 8) return n;

	var str = "";

	for(var i = len; i < 8; i++) str += '0';

	return str + n;
}

function update(elem, str)
{
	if(elem.innerHTML != str) elem.innerHTML = str;
}

function update_counter()
{
	var xhr = new XMLHttpRequest();
	xhr.onreadystatechange = function()
		{
			if(this.readyState != 4 || this.status != 200) return;

			var json = JSON.parse(this.responseText);
			update(counter, get_count_str(json["total"]));

			var langs = json["langs"];

			for(var i = 0; i < langs.length; i++) {
				var lang = langs[i];
				var elem = document.getElementById("lang_" + lang["lang"]);
				elem.style.width = lang["percentage"] + '%';
			}
		};

	xhr.open("GET", "/total_loc", true);
	xhr.send();
}
